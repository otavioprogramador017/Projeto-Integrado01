import telebot
from telebot import TeleBot
import requests
from bs4 import BeautifulSoup
import schedule
import time
from telebot import types

chave_api = '6138948203:AAGll3mwxTcfm5HLyJXQOiMOcCm4TXN0Ios'

bot = telebot.TeleBot(chave_api)

print('BOT INICIADO')


# CRIANDO MENU PRINCIPAL

@bot.message_handler(commands=['start', 'help'])
def menu_principal(mensagem):
    
    menu_principal_Keyboard = types.InlineKeyboardMarkup(row_width=2)
    bitcoin_keyboard = types.InlineKeyboardButton('BITCOIN', callback_data='bitcoin')
    etehereum_keyboard = types.InlineKeyboardButton('ETHEREUM', callback_data='ethereum')
    parar_alerta = types.InlineKeyboardButton('Parar alerta', callback_data='Parar_alerta')
    menu_principal_Keyboard.add(bitcoin_keyboard, etehereum_keyboard, parar_alerta)


    texto = 'Bem vindo ao Alert Crypto! Escolha a opção que deseja: '
    bot.send_message(mensagem.chat.id, texto, reply_markup=menu_principal_Keyboard)


# CRIANDO MENU BITCOIN
@bot.callback_query_handler(func=lambda call: call.data == 'bitcoin')
def bitcoin(callback_query):

    menu_bitcoin_keyboard = types.InlineKeyboardMarkup(row_width=2)
    valor_bitcoin = types.InlineKeyboardButton('Valor atualizado', callback_data='Valor_atualizado_btc')
    criar_alerta = types.InlineKeyboardButton('Criar alerta', callback_data='Criar_alerta_btc')
    menu_bitcoin_keyboard.add(valor_bitcoin, criar_alerta)

    bot.send_message(callback_query.message.chat.id, 'Escolha a opção que deseja: ', reply_markup=menu_bitcoin_keyboard)


# VALOR BITCOIN

@bot.callback_query_handler(func=lambda call: call.data == 'Valor_atualizado_btc')
def valor_bitcoin(callback_query):
    response = requests.get('https://www.google.com/finance/quote/BTC-BRL?sa=X&ved=2ahUKEwj3__mvg53-AhWVu5UCHf6EAFwQ-fUHegQIBhAf')
    content = response.content
    site = BeautifulSoup(content, 'html.parser')
    preco = site.find('div', attrs={'class': 'YMlKec fxKbKc'})
    texto = f'O valor atualizado da Bitcoin é de R${preco.text}'
    bot.send_message(callback_query.message.chat.id, texto)


# CRIAR ALERTA 

@bot.callback_query_handler(func= lambda call: call.data == 'Criar_alerta_btc')
def menu_alerta_btc(callback_query):
    menu_alerta_keyboard= types.InlineKeyboardMarkup(row_width=1)
    minutos_Keyboard = types.InlineKeyboardButton('MINUTOS', callback_data='minutos_btc')
    horas_Keyboard = types.InlineKeyboardButton('HORAS', callback_data='horas_btc')
    dias_Keyboard = types.InlineKeyboardButton('DIAS', callback_data='dias_btc')
    menu_alerta_keyboard.add(minutos_Keyboard, horas_Keyboard, dias_Keyboard)

    texto = '''         Escolha o um intervalo de tempo :           '''

    bot.send_message(callback_query.message.chat.id, texto, reply_markup=menu_alerta_keyboard)
    

# ESCOLHA INTERVALO MINUTOS
@bot.callback_query_handler(func= lambda call: call.data == 'minutos_btc')
def minutos_btc(callback_query):
    bot.send_message(callback_query.message.chat.id, 'Digite um número de 1 a 59 (minutos) que você deseja ser o intervalo de tempo: ')
    


def minutos(callback_query):

    lista = [str(x) for x in range(1, 60)]
    
    if callback_query.data in lista:
        return True
   
    
@bot.callback_query_handler(func=minutos)
def alerta_minutos_btc(callback_query):
    
    print('Teste')

    min = int(callback_query.data)
    
    schedule.every(min).minutes.do(valor_bitcoin, callback_query)

    while True:
        schedule.run_pending()
        time.sleep(1)


bot.polling()

#--------------------------------------------------------------------ETH----------------------------------------------------------------------------
# CRIANDO MENU ETH
@bot.callback_query_handler(func=lambda call: call.data == 'ethereum')
def ethereum(callback_query):

    menu_bitcoin_keyboard = types.InlineKeyboardMarkup(row_width=2)
    valor_ethereum = types.InlineKeyboardButton('Valor atualizado', callback_data='Valor_atualizado_eth')
    criar_alerta = types.InlineKeyboardButton('Criar alerta', callback_data='Criar_alerta_eth')
    menu_bitcoin_keyboard.add(valor_ethereum, criar_alerta)

    bot.send_message(callback_query.message.chat.id, 'Escolha a opção que deseja: ', reply_markup=menu_bitcoin_keyboard)


#Valor ETH

@bot.message_handler(commands=['Ethereum'])
def Ethereum(mensagem):
    response = requests.get('https://www.google.com/finance/quote/ETH-BRL')
    content = response.content
    site = BeautifulSoup(content, 'html.parser')
    preco = site.find('div', attrs={'class': 'YMlKec fxKbKc'})
    variacao = site.find('div', attrs={'class': 'JwB6zf'})
    texto = f'O valor atualizado da Ethereum é de R${preco.text} \nA variação do ETH é de {variacao.text}'
    bot.send_message(mensagem.chat.id, texto)

# CRIAR ALERTA 

@bot.callback_query_handler(func= lambda call: call.data == 'Criar_alerta_eth')
def menu_alerta_eth(callback_query):
    menu_alerta_keyboard= types.InlineKeyboardMarkup(row_width=1)
    minutos_Keyboard = types.InlineKeyboardButton('MINUTOS', callback_data='minutos_eth')
    horas_Keyboard = types.InlineKeyboardButton('HORAS', callback_data='horas_eth')
    dias_Keyboard = types.InlineKeyboardButton('DIAS', callback_data='dias_eth')
    menu_alerta_keyboard.add(minutos_Keyboard, horas_Keyboard, dias_Keyboard)

    texto = '''         Escolha o um intervalo de tempo :           '''

    bot.send_message(callback_query.message.chat.id, texto, reply_markup=menu_alerta_keyboard)

# ESCOLHA INTERVALO MINUTOS
@bot.callback_query_handler(func= lambda call: call.data == 'minutos_eth')
def minutos_eth(callback_query):
    bot.send_message(callback_query.message.chat.id, 'Digite um número de 1 a 59 (minutos) que você deseja ser o intervalo de tempo: ')
    


def minutos(callback_query):

    lista = [str(x) for x in range(1, 60)]
    
    if callback_query.data in lista:
        return True
   
    
@bot.callback_query_handler(func=minutos)
def alerta_minutos_eth(callback_query):
    
    print('Teste')

    min = int(callback_query.data)
    
    schedule.every(min).minutes.do(valor_bitcoin, callback_query)

    while True:
        schedule.run_pending()
        time.sleep(1)


bot.polling()
