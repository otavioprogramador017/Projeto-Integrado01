# chat telegram
import telebot
from telebot import TeleBot

# web scraping
import requests
from bs4 import BeautifulSoup

# agendar tarefas
import schedule
import time

# traduzir string
from googletrans import Translator



chave_api = '6138948203:AAGll3mwxTcfm5HLyJXQOiMOcCm4TXN0Ios'

bot = telebot.TeleBot(chave_api)

print('BOT INICIADO')


# MENU PRINTCIPAL

@bot.message_handler(commands=['start', 'help'])       
def responder(mensagem):
  texto = """
  Bem vindo ao Alert Crypto, selecione a criptmoeda desejada:
  
  /Bitcoin
  
  /Ethereum 

  /Parar_alerta
  
  Sempre que desejar utilizar esse menu digite /start ou /help

  """
  
  bot.send_message(mensagem.chat.id, texto)

# ------------------------ ----------------------- CÓDIGO BITCOIN -----------------------------------------------------------------------


# MENU PRINCIPAL DE OPÇÕES

@bot.message_handler(commands=['Bitcoin'])
def Bitcoin(mensagem):
    texto = """
    Seleciona a opção que deseja :

/Valor_atualizado

/Criar_alerta 


"""
    bot.send_message(mensagem.chat.id, texto)
  


# WEB SCRAPING BITCOIN

@bot.message_handler(commands=['Valor_atualizado'])
def valor_bitcoin(mensagem):
    response = requests.get('https://www.google.com/finance/quote/BTC-BRL?sa=X&ved=2ahUKEwj3__mvg53-AhWVu5UCHf6EAFwQ-fUHegQIBhAf')
    content = response.content
    site = BeautifulSoup(content, 'html.parser')
    preco = site.find('div', attrs={'class': 'YMlKec fxKbKc'})
    texto = f'O valor atualizado da Bitcoin é de R${preco.text}'
    bot.send_message(mensagem.chat.id, texto)



# MENU OPÇÕES DE INTERVALO DE TEMPO

@bot.message_handler(commands=['Criar_alerta'])
def alerta_bitcoin(mensagem):
    texto = """
    Selecione o intervalo de tempo :

    /Minutos

    /Horas

    /Dias

    """
    
    bot.send_message(mensagem.chat.id, texto)




# ESCOLHA INTERVALO ALERTA MINUTOS

@bot.message_handler(commands=['Minutos'])
def minutos_btc(mensagem):

    bot.send_message(mensagem.chat.id, 'Digite um número de 1 a 59 (minutos) que você deseja ser o intervalo de tempo: ')


def minutos(mensagem):
    lista = [range(1,60)]
    
    if mensagem.text in lista:
        min = int(mensagem.text)
        
        if min in range (1, 60):
            return True


@bot.message_handler(func=minutos)
def alerta_minutos_btc(mensagem):
    min = int(mensagem.text)
    
    schedule.every(min).minutes.do(valor_bitcoin, mensagem).tag('btc')

    while True:
        schedule.run_pending()
        time.sleep(1)


# ESCOLHA INTERVALO ALERTA HORAS

@bot.message_handler(commands=['Horas'])
def horas_btc(mensagem):

    bot.send_message(mensagem.chat.id, 'Digite um número de 1 a 23 (horas) que você deseja ser o intervalo de tempo: ')

def horas (mensagem):
    lista = [range(1,24)]
    
    if mensagem.text in lista:
        horas = int(mensagem.text)

        if horas in range(1, 24):
            return True
    
@bot.message_handler(func=horas)
def alerta_horas_btc(mensagem):
    horas = int(mensagem.text)

    schedule.every(horas).hours.do(valor_bitcoin, mensagem).tag('btc')

    while True:
        schedule.run_pending()
        time.sleep(1)



# ESCOLHA INTERVALO DIAS 

@bot.message_handler(commands=['Dias'])
def dias_btc(mensagem):
    texto = '''
    Digite um número de 1 a 6 (dias) que você deseja ser o intevalo entre os alertas :

Ou digite o dia da semana que você deseja receber os alertas :

(para os dias de segunda a sexta adicione -feira para funcionar, ex: segunda-feira) :
    '''

    bot.send_message(mensagem.chat.id, texto)

# FUNÇÃO PARA RETORNAR TRUE
def dias(mensagem):
    lista = ['1', '2', '3', '4', '5', '6']
    dias = ['segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado', 'domingo']

    if mensagem.text in lista or mensagem.text in dias:
        return True

@bot.message_handler(func=dias)
def alerta_dias_btc(mensagem):
    lista = ['1', '2', '3', '4', '5', '6']
    dias = ['segunda-feira', 'terça-feira', 'quarta-feira', 'quinta-feira', 'sexta-feira', 'sábado', 'domingo']

    if mensagem.text in dias:
        
        if mensagem.text == 'segunda-feira':
            schedule.every().monday.at('08:00').do(valor_bitcoin, mensagem)

            while True:
                schedule.run_pending()
                time.sleep(1)
        
        elif mensagem.text == 'terça-feira':
            schedule.every().tuesday.at('08:00').do(valor_bitcoin, mensagem)

            while True:
                schedule.run_pending()
                time.sleep(1)

        elif mensagem.text == 'quarta-feira':
            schedule.every().wednesday.at('08:00').do(valor_bitcoin, mensagem)

            while True:
                schedule.run_pending()
                time.sleep(1)

        elif mensagem.text == 'quinta-feira':
            schedule.every().thursday.at('08:00').do(valor_bitcoin, mensagem)

            while True:
                schedule.run_pending()
                time.sleep(1)

        elif mensagem.text == 'sexta-feira':
            schedule.every().friday.at('08:00').do(valor_bitcoin, mensagem)

            while True:
                schedule.run_pending()
                time.sleep(1)

        elif mensagem.text == 'sábado':
            schedule.every().saturday.at('08:00').do(valor_bitcoin, mensagem)

            while True:
                schedule.run_pending()
                time.sleep(1)

        elif mensagem.text == 'domingo':
            schedule.every().sunday.at('08:00').do(valor_bitcoin, mensagem)

            while True:
                schedule.run_pending()
                time.sleep(1)

    elif mensagem.text in lista:
        d = int(mensagem.text)
        print(d)


# PARAR OS ALERTAS ----------------------------------------------

@bot.message_handler(commands=['Parar_alerta'])
def parar_alerta(mensagem):

    for job in schedule.jobs:
        schedule.cancel_job(job)

    bot.send_message(mensagem.chat.id, 'Alerta cancelado')    
    print('Alerta Cancelado')

#-----------------------------------------------------------------


bot.polling()